'use strict';

angular.module( 'vacationApp' ).service(
    'CalendarService', [
      '$http',
      'configuration',
      '$cookies',
      '$location',
      'AuthorizationService',
      function ( $http, configuration, $cookies, $location, AuthorizationService ) {
        var date = new Date();

        return {
          year: date.getFullYear(),                                   //selected year
          month: date.getMonth() + 1,                                 //selected month
          searchName: "",
          days: function ( year, month ) {                              //count of days in selected month
            if ( year === undefined || year === null ) {
              return new Date( this.year, this.month, 0 ).getDate();
            } else {
              return new Date( year, month, 0 ).getDate();
            }
          },
          daysRange: function ( days ) {                                    //days-range array
            var result = [];

            if ( ! days ) {
              days = this.days();
            }

            for ( var i = 1, a = days; i <= a; i ++ ) {
              result.push( i );
            }

            return result;
          },
          getDay: function ( day, year, month ) {
            if ( year === undefined || year === null ) {
              return new Date( this.year, this.month - 1, day - 1 ).getDay();
            } else {
              return new Date( year, month - 1, day - 1 ).getDay();
            }
          },

          getPrettyDate: function ( day, year, month ) {
            var date = new Date( year, month, day );
            return date;
          }
        };
      }
    ]
);
