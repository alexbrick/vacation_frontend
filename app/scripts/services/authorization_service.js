'use strict';

/**
 * A Service that is responsible for login. It contains credentials of the user, allowed AOs,
 * allowed Topics and permissions for charts.
 */
angular.module( 'vacationApp' )
    .factory(
    'AuthorizationService', [
      '$http',
      '$rootScope',
      '$cookies',
      '$route',
      '$location',
      '$window',
      'configuration',
      'growl',
      function ( $http, $rootScope, $cookies, $route, $location, $window, configuration, growl ) {
        var auth = {};

        auth.loggedIn = false;

        auth.isLoggedIn = function () {
          return auth.loggedIn;
        };

        auth.login = function ( username, password ) {

          return $http.post(
              configuration.auth_url,
              $.param( {username: username, password: password} ),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
          )
              .success(
              function ( data ) {
                var response = data;
                $cookies.put( 'token', data.token );
                setAuthHeader( data['token'] );

                auth.loggedIn = true;

                response.email = username;
                delete response.token;

                localStorage.setItem( 'user', JSON.stringify( response ) );

                $http.get( configuration.api_url + 'users/' + response.id + '/' )
                    .success(
                    function ( data ) {
                      localStorage.setItem( 'user', JSON.stringify( data ) );

                      setAuthHeader( $cookies.get( 'token' ) );
                    }
                    ).error(
                        function ( data, status, headers, config ) {

                        }
                    );
                    if ( response.group_code == '2' ) {
                      $location.path( '/manager/' );
                    } else if ( response.group_code == '3' ) {
                      $location.path( '/admin/' );
                    } else {
                      $location.path( '/user/' );
                    }


              }
          );

        };

        auth.initSession = function ( token ) {

          setAuthHeader( token );
          var user = JSON.parse( localStorage.getItem( 'user' ) );
          var count = 0;
          auth.loggedIn = true;

          if ( user.group_code == '2' || user.group_code == '3' ) {
            var $admin_count = $cookies.get( 'loggedInAdminCount' );

            ($admin_count) ? $cookies.put( 'loggedInAdminCount', (parseInt( $admin_count ) + 1) ) :
                $cookies.put( 'loggedInAdminCount', count );

            if ( ! ($admin_count > 2) && user.group_code == '2' ) {
              $location.path( '/manager/' );
            }

          } else if ( ! ($admin_count > 2) && user.group_code == '3' ) {
            $location.path( '/admin/' );

          } else if ( user.group_code == '1' ) {
            //debugger;
            var $user_count = $cookies.get( 'loggedInUser' );

            ($user_count) ? $cookies.put( 'loggedInUser', (parseInt( $user_count ) + 1) ) :
                $cookies.put( 'loggedInUser', count );

            if ( ! ($user_count > 2) ) {
              $location.path( '/profile/edit/' );
            } else {
              $location.path( '/user/' );
            }
          }
        };

        var setAuthHeader = function ( token ) {
          $http.defaults.headers.common.Authorization = 'Token ' + token;
        };
        auth.logout = function () {
          auth.loggedIn = false;
          localStorage.removeItem( 'user' );
          $cookies.remove( "token" );
          $cookies.put( 'loggedIn', false );
          $cookies.put( 'loggedInAdminCount', 0 );
          $cookies.put( 'loggedInUser', 0 );
          delete $http.defaults.headers.common.Authorization;
          $location.path( '/login/' );
        };
        return auth;
      }
    ]
);