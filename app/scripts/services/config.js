'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    auth_url: 'http://sub1.lt01test.tk/api/api-token-auth/',
    api_url: 'http://sub1.lt01test.tk/api/'
  });
