'use strict';

angular.module( 'vacationApp' ).service(
    "VacationService", [
      '$http',
      'configuration',
      '$cookies',
      function ( $http, configuration, $cookies ) {
        this.addVacation = function ( vacation ) {
          return $http.post( configuration.api_url + 'vacations/', vacation );
        };

        this.getVacations = function () {
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.get( configuration.api_url + 'vacations/' )
        };

        this.getVacation = function ( id ) {
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.get( configuration.api_url + 'vacations/' + id + '/' );
        };

        this.getUserByMonth = function ( month, year ) {
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.get( configuration.api_url + 'vacations/?month=' + month + '&year=' + year );

        };

        this.getVacationsByUser = function ( user_id ) {
          return $http.get( configuration.api_url + 'users/' + user_id + '/vacations/' );
        };


        this.changeState = function ( id, setState, comment ) {
          //debugger;
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.put(
              configuration.api_url + 'vacations/' + id + '/',
              {
                state: setState,
                comment_admin: comment
              }
          );
        };

        this.addComment = function ( id, comment ) {
          //debugger;
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.put( configuration.api_url + 'vacations/' + id + '/', {comment_admin: comment} );
        };

        this.defineRangeFromData = function ( days, month, year ) {
          var range = '';
          var endDate = null;
          var startDate = new Date( year, month[0], days[0][0] );
          //Check if this month Aug
          if ( month[0] == 8 ) {
            startDate = new Date( year, month[0] + 1, days[0][0] );
          }

          if ( month[1] === null ) {
            endDate = new Date( year, month[0], days[0][days[0].length - 1] );
          } else {
            endDate = new Date( year, month[1], days[1][days[1].length - 1] );
          }

          startDate =
              ('0' + (startDate.getDate())).slice( - 2 ) + '.' + ('0' + (startDate.getMonth())).slice( - 2 ) + '.' +
              startDate.getFullYear();
          endDate = ('0' + (endDate.getDate())).slice( - 2 ) + '.' + ('0' + (endDate.getMonth())).slice( - 2 ) + '.' +
              endDate.getFullYear();

          range = startDate + ' - ' + endDate;
          return range;
        };
        this.page = function ( page ) {
          setAuthHeader( $cookies.get( 'token' ) );
          return $http.get( page );
        };
        this.setDateDifference = function( data ) {
          //debugger;

          if(angular.isArray(data)){
            angular.forEach(data, function(value) {
              _getDateDifference(value);
            });
          }else{
            _getDateDifference(data);
          }

          function _getDateDifference(value){
            var date1 = new Date(value.date_start);
            var date2 = new Date(value.date_end);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            value.diff_days = diffDays;
          }

        };

        var setAuthHeader = function ( token ) {
          $http.defaults.headers.common.Authorization = 'Token ' + token;
        };

      }
    ]
);