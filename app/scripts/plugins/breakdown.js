'use strict';

/**
 * Renders a breakdown visualization.
 * @param   {Object} el   DOM element, where this chart is supposed to be added.
 * @param   {Object} data object with topics and sentiment values.
 * @returns {Object} instance of the chart.
 */
var SCBreakdown = function (el, data) {
  var publicAPI = {
    render: render
  };

  /* This chart just manipulates with divs and classes. It adds a table-like structure.
   * Each sentiment bar receives a width in %, based on sentiment data.
   */
  function render (id) {
    var barWidth = 100; // maximum value for bar width - 100%
    var maxValue = Math.max(d3.max(data, function(d) {return d.posBig; }), d3.max(data, function(d) { return d.negBig; }));
    var x = d3.scale.linear().range([0, 100]).domain([0, maxValue]);

    if(typeof id != 'undefined'){
      var foundId = $.map(data, function(object) {
          return object.id == id ? object : null;
      });
      if(foundId){
        data = foundId;
      }
    }

    var svg = d3.select(el).append('div')
      .attr('class', 'breakdown-container');

    var row = svg.selectAll('.breakdown-row')
      .data(data)
      .enter()
        .append('div')
        .attr('id', function(d) { return d.id; })
        .attr('class', 'breakdown-row');

    row.append('div')
      .attr('class', 'topic-name')
      .text(function(d) { return d.name; });

    var rowVis = row.append('div')
      .attr('class', 'rowVis');

    rowVis.append('div')
      .text(function(d) { return d.negBig + ' / ' + d.negSmall; })
      .attr('class', 'neg value');

    var negBar = rowVis.append('div')
      .attr('class', 'bar-container');

    var posBar = rowVis.append('div')
      .attr('class', 'bar-container');

    rowVis.append('div')
      .text(function(d) { return d.posBig + ' / ' + d.posSmall; })
      .attr('class', 'pos value');

    negBar.append('div')
      .style('width', barWidth + '%')
      .attr('class', 'bar neg background');

    negBar.append('div')
      .style('width', function (d) { return x(d.negBig) + '%'; })
      .attr('class', 'bar neg big');

    negBar.append('div')
      .style('width', function (d) {return x(d.negSmall) + '%'; })
      .attr('class', 'bar neg small');

    posBar.append('div')
      .style('width', barWidth + '%')
      .attr('class', 'bar pos background');

    posBar.append('div')
      .style('width', function (d) {return x(d.posBig) + '%'; })
      .attr('class', 'bar pos big');

    posBar.append('div')
      .style('width', function (d) {return x(d.posSmall) + '%'; })
      .attr('class', 'bar pos small');
  }

  return publicAPI;
};
