'use strict';

angular.module( 'vacationApp' ).controller(
    'AuthorizationCtrl', function ( $scope, $location, $routeParams, AuthorizationService, $http, configuration ) {

      $scope.error = null;
      $scope.login = function ( isValid ) {

        if ( isValid ) {
          AuthorizationService.login( $scope.email, $scope.password ).error(
              function ( data, status, headers, config ) {
                $scope.error = data.non_field_errors.toString();
              }
          );

        }
      };

    }
);
