'use strict';

angular.module('vacationApp').controller('SettingsCtrl', ['$scope', '$rootScope', 'UsersService', 'MailService', 'ngDialog', function ($scope, $rootScope, UsersService, MailService, ngDialog) {
    $scope.userList = null;
    $scope.predicate = 'last_name';
    $scope.reverse = true;
    $scope.loaded = false;
    $scope.current_user =  JSON.parse(localStorage.getItem('user'));
    function getUserList() {
        UsersService.getUsers()
            .success(function (data) {
                $scope.userList = data.results;
                $scope.prev = (data.previous != null) ? data.previous : '';
                $scope.next = (data.next != null) ? data.next : '';
                $scope.loaded = true;
            })
            .error(function (err) {
                throw new Error(err);
            });
    }

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    getUserList();

    $scope.deleteUser = function (id) {
        $scope.$emit('deleteUser');
        ngDialog.openConfirm({
            template: '<p>Вы действительно хотите удалить этого пользователя ?</p>' +
                '<div style="text-align:right;">' +
                '<button type="button" class="btn btn-default" ng-click="closeThisDialog(0)">Нет&nbsp;' +
                '<button type="button" style="margin-left: 5px;" class="btn btn-primary" ng-click="confirm(1)">Да' +
                '</button></div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).then(function (value) {
            UsersService.deleteUser(id)
                .success(function (data) {
                    getUserList();
                })
                .error(function (err) {
                    throw err;
                });
        }, function (value) {
            //Do something
        });

    };


    $scope.subscriber = {};
    $scope.mailList = {};
    $scope.email = {};
    $scope.mail = {};
    $scope.prev = '';
    $scope.next = '';
    $scope.email.prev = '';
    $scope.email.next = '';

    $scope.$on('anyChanges', function (event, data) {
        MailService.getMailList()
            .success(function (data, status) {
                $scope.mailList = data.results;
            })
            .error(function (err, status) {
                throw err;
            });
    });

    //Init
    if ( $scope.current_user.group_code===3) {
        $scope.$emit('anyChanges');
    }



    $scope.addNewMail = function (isValid) {
        if (isValid) {
            MailService.addNewMail($scope.subscriber)
                .success(function (data, status) {
                    $scope.$emit('anyChanges');
                })
                .error(function (error, status) {
                    throw err;
                });
        }
    };

    $scope.deleteMail = function (id) {
        ngDialog.openConfirm({
            template: '<p>Вы действительно хотите удалить этот адресс ?</p>' +
                '<div style="text-align:right;">' +
                '<button type="button" class="btn btn-default" ng-click="closeThisDialog(0)">Нет&nbsp;' +
                '<button type="button" style="margin-left: 5px;" class="btn btn-primary" ng-click="confirm(1)">Да' +
                '</button></div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).then(function (value) {
            MailService.deleteMail(id)
                .success(function (data, status) {
                    $scope.$emit('anyChanges');
                })
                .error(function (err, status) {
                    throw err;
                });
        }, function (value) {
            //Do something
        });


    };

    $scope.changeState = function (id) {
        MailService.updateMail(id, this.mail.state)
            .success(function (data, status) {
                // $scope.$emit('anyChanges');
            })
            .error(function (err) {
                throw err;
            });
    };

    $scope.changeAction = function (id, action) {
        MailService.updateMailAction(id, action, action)
            .success(function (data, status) {
                // $scope.$emit('anyChanges');
            })
            .error(function (err) {
                throw err;
            });
    };

    $scope.page = function (type) {
        $scope.$emit('page');
        if (type == 'next') {
            UsersService.page($scope.next).success(function (data, status) {
                $scope.userList = data.results;
                $scope.prev = (data.previous != null) ? data.previous : '';
                $scope.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        } else if (type == 'prev') {
            UsersService.page($scope.prev).success(function (data, status) {
                $scope.userList = data.results;
                $scope.prev = (data.previous != null) ? data.previous : '';
                $scope.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        }
        return false;
    }

    $scope.email_page = function (type) {
        $scope.$emit('page');
        if (type == 'next') {
            MailService.page($scope.next).success(function (data, status) {
                $scope.mailList = data.results;
                $scope.email.prev = (data.previous != null) ? data.previous : '';
                $scope.email.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        } else if (type == 'prev') {
            MailService.page($scope.prev).success(function (data, status) {
                $scope.mailList = data.results;
                $scope.email.prev = (data.previous != null) ? data.previous : '';
                $scope.email.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        }
        return false;
    }
}]);