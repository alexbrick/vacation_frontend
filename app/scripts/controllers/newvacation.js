angular.module( 'vacationApp' )
    .controller(
    'NewVacationCtrl', [
      '$scope',
      'VacationService',
      'ngDialog',
      'UsersService',
      '$cookies',
      '$timeout',
      function ( $scope, VacationService, ngDialog, UsersService, $cookies, $timeout ) {

        $scope.range = {
          start: null,
          end: null
        };


        $scope.vacation = {};
        $scope.loaded = true;
        $scope.SubmitForm;
        $scope.errors = [];
        $scope.user = JSON.parse( localStorage.getItem( 'user' ) );
        $scope.daysNumber = 0;
        $scope.daysNumberLeft = 0;

        $scope.watchForDates = watchForDates;
        $scope.initialize = initialize;

        $scope.isDuplicated = false;

        initialize();

        function initialize() {
          if ( $cookies.get( 'token' ) ) {
            var user = JSON.parse( localStorage.getItem( 'user' ) );

            $scope.user = user;

            UsersService.getUserVacations( user.id )
                .success(
                function(data) {

                  //console.log('data', data);

                  if (data.length) {
                    $scope.daysNumberLeft = data[0].possible_days;
                  }
                })
                .error(
                function(data) {
                  console.log('Error with getting Vacations data!', data);
                })
          }
        }

        function preventRangeDuplication(userVacations){
          var
            start = $scope.range.start,
            end = $scope.range.end,
            oldStart, oldEnd, oldState, cond1, cond2, cond3;

          for (var i = 0; i < userVacations.length; i++){
            oldStart = new Date(userVacations[i].date_start).getTime();
            oldEnd = new Date(userVacations[i].date_end).getTime();
            oldState = userVacations[i].state;
            // console.warn(oldStart, oldEnd, oldState);

            cond1 = start > oldEnd;
            cond2 = end < oldStart;
            cond3 = oldState == 31;

            if (cond1 || cond2 || cond3) {
              //console.log('yes, you can');
            } else {
              //console.log('NO, you can not');
              return true;
            }

          }

          return false;

        }


        //Datepicker validation
        function watchForDates() {
          var user = JSON.parse( localStorage.getItem( 'user' ) );
          $scope.SubmitForm = true;

          if ( ($scope.range.end > $scope.range.start) &&
              isRangeCorrect( $scope.range.start, $scope.range.end ) ) {
            $scope.SubmitForm = false;
          }

          if ( ! isRangeCorrect( $scope.range.start, $scope.range.end ) ) {
            if ( $scope.errors.length ) {
              $scope.errors.shift();
            }
            $scope.errors.push( 'Промежуток не должен превышать 14 дней!' );
          } else {
            if ( $scope.errors.length ) {
              $scope.errors.shift();
            }
          }


          UsersService.getUserVacations( user.id ).then(function(promised){
            var doPrevent = preventRangeDuplication(promised.data);
            if (doPrevent) {
              if ($scope.errors.length ){
                $scope.errors.shift();
              }
              $scope.errors.push('Вы уже взяли отпуск на этот период');
              $scope.SubmitForm = true;
            }

          });
        }

        $scope.$watch(function () {return $scope.range.start}, watchForDates);
        $scope.$watch(function () {return $scope.range.end}, watchForDates);

        function isRangeCorrect( start, end ) {
          var oToday = start;
          var oDeadLineDate = end;

          var days = $scope.daysNumber = oDeadLineDate > oToday ? Math.ceil( (oDeadLineDate - oToday) / (1000 * 60 * 60 * 24) ) : null;

          return days < 14;
        }

        //defines array with year, months, days
        function makeRange() {
          var result = {}
          /*
           year: 2015,
           month: [3, 4],
           days: [[30, 31], [1,2,3,4,5,6]],
           */;

          var startDate = new Date( $scope.range.start );
          var endDate = new Date( $scope.range.end );

          var yearNum = startDate.getFullYear();

          var startDay = startDate.getDate();
          var endDay = endDate.getDate();

          var startMonthNum = (startDate.getMonth() + 1);
          var endMonthNum = (endDate.getMonth() + 1);

          var daysInStartDate = new Date( yearNum, startMonthNum, 0 ).getDate();

          if ( startMonthNum === endMonthNum ) {
            month = [
              startMonthNum,
              null
            ];
          } else {
            month = [
              startMonthNum,
              endMonthNum
            ];
          }
          //define days array
          var days = [
            [],
            []
          ];

          if ( month[1] === null ) {
            for ( var i = startDay;
                  i <= endDay;
                  i ++ ) {
              days[0].push( i );
            }
            days[1] = null;
          } else {
            for ( var i = startDay;
                  i <= daysInStartDate;
                  i ++ ) {
              days[0].push( i );
            }

            for ( var j = 1;
                  j <= endDay;
                  j ++ ) {
              days[1].push( j );
            }
          }

          result.year = yearNum;
          result.month = month;
          result.days = days;

          return result;
        }

        $scope.showResult = function () {

          console.log('show result');

          var range = makeRange();

          $scope.vacation.year = range.year;
          $scope.vacation.month = range.month;
          $scope.vacation.days = range.days;

          var params = {
            "date_end": vacationDay( $scope.range.end ),
            "date_start": vacationDay( $scope.range.start ),
            "user_id": $scope.user.id,
            "comment_user": $scope.vacation.comment_user
          };


          //



          VacationService.addVacation( params )
              .success(
                function ( data, status ) {
                  ngDialog.open(
                      {
                        template: '<p>Заявка отправлена!</p>',
                        plain: true
                      }
                  );
                  // Clear input fields
                  $scope.range = {
                    start: null,
                    end: null
                  };
                  $scope.vacation = {};

                  initialize();
              })
              .error(
                function ( data, status, headers, config ) {
                  if ($scope.errors.length) {
                    $scope.errors = [];
                  }

                  if ( data.non_field_errors ) {
                    $scope.errors.push( data.non_field_errors[0].toString().replace( /[\[\]']+/g, '' ) );
                  } else if ( data.error ) {
                    angular.forEach( data.error, function(item) {
                      $scope.errors.push( item.toString().replace( /[\[\]']+/g, '' ) );
                    });
                  }

                  $scope.SubmitForm = true; // Disable button
                }
              );
        };

        function vacationDay( dateValue ) {
          var date = new Date( dateValue );
          var yyyy = date.getFullYear().toString();
          var mm = (date.getMonth() + 1).toString();
          var dd = date.getDate().toString();
          var mmChars = mm.split( '' );
          var ddChars = dd.split( '' );
          var datestring = yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' +
              (ddChars[1] ? dd : "0" + ddChars[0]);

          return datestring;
        }

        /* DATEPICKER OPTIONS */
        $scope.today = function() {
          $scope.range.start = new Date();
        };
        $scope.today();

        $scope.clear = function() {
          $scope.range.start = null;
        };

        $scope.options_first = {
          dateDisabled: disabled,
          customClass: getDayClass,
          minDate: new Date(),
          showWeeks: false
        };

        $scope.options_second = {
          dateDisabled: disabled,
          customClass: getDayClass,
          minDate: $scope.range.start,
          showWeeks: false
        };

        $scope.$watch(function () {return $scope.range.start}, function() {
          //debugger;
          if ($scope.range.start) {
            $scope.options_second.minDate = new Date($scope.range.start.getTime() + 86400000);
            $scope.range.end = new Date($scope.range.start.getTime() + 86400000);
          }
        });

        $scope.open1 = function() {
          $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
          $scope.popup2.opened = true;
        };
        $scope.popup1 = {
          opened: false
        };

        $scope.popup2 = {
          opened: false
        };


        // Disable weekend selection
        function disabled(data) {
          var date = data.date,
              mode = data.mode;
          return mode === 'day' && (date.getTime() < (new Date()).getTime());
        }

        //$scope.toggleMin = function() {
        //  $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        //  $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        //};
        //
        //$scope.toggleMin();

        $scope.open1 = function() {
          $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
          $scope.popup2.opened = true;
        };

        //$scope.setDate = function(year, month, day) {
        //  $scope.range.start = new Date(year, month, day);
        //};

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd-MM-yyyy',];
        $scope.format = $scope.formats[4];
        $scope.altInputFormats = ['M!/d!/yyyy'];


        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
          {
            date: tomorrow,
            status: 'full'
          },
          {
            date: afterTomorrow,
            status: 'partially'
          }
        ];

        function getDayClass(data) {
          var date = data.date,
              mode = data.mode;
          if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
              var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

              if (dayToCheck === currentDay) {
                return $scope.events[i].status;
              }
            }
          }

          return '';
        }


      }
    ]
);