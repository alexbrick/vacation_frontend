angular.module('vacationApp')
  .controller('UserProfileCtrl',
    ['$scope', '$rootScope', 'UsersService', 'ngDialog',
      function ($scope, $rootScope, UsersService, ngDialog) {
  var user = JSON.parse(localStorage.getItem('user'));
  $scope.loaded = false;
  $scope.user = user;

  $scope.first_name = user.first_name;
  $scope.last_name = user.last_name;
  $scope.rank = user.rank;
  $scope.id = user.id;
  $scope.newEmail = user.email;
  $scope.loaded = true;

  $scope.updateUser = function (isValid) {
    if (isValid) {
      var first_name = $scope.first_name;
      var last_name = $scope.last_name;

      var updSuccess = UsersService.updateUser(
        $scope.id,
        {
          'last_name': last_name,
          'email': $scope.newEmail,
          'first_name': first_name
        }
      );

      updSuccess
        .success(function(){
          showMsg('Данные успешно обновлены')
        })
        .error(function(){
          showMsg('Ошибка: <br />Пользователь с таким емаилом уже существует в БД')
        });


      function showMsg(msg){
        ngDialog.open({
          template: '<p>' + msg + '</p> ',
          plain: true
        });
      }


    }
  };



}]);
