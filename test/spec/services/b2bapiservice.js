'use strict';

describe('Service: B2BApiService', function () {

  // load the service's module
  beforeEach(module('newsboardWebApp'));

  // instantiate service
  var B2BApiService;
  beforeEach(inject(function (_B2BApiService_) {
    B2BApiService = _B2BApiService_;
  }));

  it('should do something', function () {
    expect(!!B2BApiService).toBe(true);
  });

});
