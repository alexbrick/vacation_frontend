Vacation Application
===================

## Setup

* Install [NodeJS](http://nodejs.org)
* Install [Grunt](http://gruntjs.com)
* Install [Bower](http://bower.io)
* Run `npm install` to install NodeJS dependencies
* Run `bower install` to install Bower dependencies

## Running

Use the following command to start the service:

```sh
$ grunt serve
```

Switching api environments:

* Localhost
```sh
$ grunt local
```

* Development
```sh
$ grunt dev
```
* Production
```sh
$ grunt prod
```

## Building

* Build development version
```sh
$ grunt build_dev
```

* Build production version
```sh
$ grunt build_prod
```