#!/bin/sh

OLD_PWD="$PWD"
FRONT_DIR="$(dirname "$0")"
WWW_DIR="/var/www/sub1.lt01test.tk"

cd "$FRONT_DIR"

npm install
bower install

grunt prod
grunt build_prod

rm -fr "$WWW_DIR"/*
cp -a "$FRONT_DIR"/dist/. "$WWW_DIR"

find "$WWW_DIR" -type d -or -type f -print0 | xargs -0 chown usr01:www-data
find "$WWW_DIR" -type d -print0 | xargs -0 chmod 755
find "$WWW_DIR" -type f -print0 | xargs -0 chmod 644

cd "$OLD_PWD"